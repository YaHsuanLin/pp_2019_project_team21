#include <stdio.h>
#include <stdlib.h>

#include <omp.h>
#include "kmeans.h"

// distance between two points
__inline static
float euclid_dist_2(int numdims, float *coord1, float *coord2) {
    int i;
    float ans = 0.0;

    for (i = 0; i < numdims; i++)
        ans += (coord1[i] - coord2[i]) * (coord1[i] - coord2[i]);

    return(ans);
}

// find the nearest cluster for each point
__inline static
int find_nearest_cluster(int numClusters, int numCoords, float  *object, float **clusters) {
    int   index = 0, i;
    float min_dist = euclid_dist_2(numCoords, object, clusters[0]);

    for (i = 1; i < numClusters; i++) {
        float dist = euclid_dist_2(numCoords, object, clusters[i]);
        
        if (dist < min_dist) { /* find the min and its array index */
            min_dist = dist;
            index    = i;
        }
    }
    return(index);
}

/*----< omp_kmeans() >------------------------------------------------*/
// return an array of cluster centers of size [numClusters][numCoords]
float** omp_kmeans(float **objects, int numCoords, int numObjs, int numClusters, float threshold, int *membership) {
    int      i, j, index, loop = 0;
    int     *newClusterSize;        /* [numClusters]: no. objects assigned in each new cluster */
    float    delta;                 /* % of objects change their clusters */
    float  **clusters;              /* out: [numClusters][numCoords] */
    float  **newClusters;           /* [numClusters][numCoords] */
    int      nthreads;              /* no. threads */
    double   timing;

    nthreads = omp_get_max_threads();

    /* allocate a 2D space for returning variable clusters[] (coordinates of cluster centers) */
    clusters    = (float**) malloc(numClusters * sizeof(float*));
    assert(clusters != NULL);
    clusters[0] = (float*)  malloc(numClusters * numCoords * sizeof(float));
    assert(clusters[0] != NULL);
    for (i = 1; i < numClusters; i++)
        clusters[i] = clusters[i-1] + numCoords;

    /* pick first numClusters elements of objects[] as initial cluster centers */
    for (i = 0; i < numClusters; i++)
        for (j = 0; j < numCoords; j++)
            clusters[i][j] = objects[i][j];

    for (i = 0; i < numObjs; i++) 
        membership[i] = -1;

    /* need to initialize newClusterSize and newClusters[0] to all 0 */
    newClusterSize = (int*) calloc(numClusters, sizeof(int));
    assert(newClusterSize != NULL);
    newClusters    = (float**) malloc(numClusters * sizeof(float*));
    assert(newClusters != NULL);
    newClusters[0] = (float*)  calloc(numClusters * numCoords, sizeof(float));
    assert(newClusters[0] != NULL);
    
    for (i = 1; i < numClusters; i++)
        newClusters[i] = newClusters[i-1] + numCoords;

    if (_debug) 
        timing = omp_get_wtime();

    do {
        delta = 0.0;
        
        #pragma omp parallel for private(i,j,index) \
                firstprivate(numObjs,numClusters,numCoords) \
                shared(objects,clusters,membership,newClusters,newClusterSize) \
                schedule(static) reduction(+:delta)
        for (i = 0; i < numObjs; i++) {
            /* find the array index of nestest cluster center */
            index = find_nearest_cluster(numClusters, numCoords, objects[i], clusters);

            /* if membership changes, increase delta by 1 */
            if (membership[i] != index) 
                delta += 1.0;

            /* assign the membership to object i */
            membership[i] = index;

            /* update new cluster centers : sum of objects located within */
            #pragma omp atomic
            newClusterSize[index]++;
            for (j = 0; j < numCoords; j++)
                #pragma omp atomic
                newClusters[index][j] += objects[i][j];
        }

        /* average the sum and replace old cluster centers with newClusters */
        for (i = 0; i < numClusters; i++) {
            for (j = 0; j < numCoords; j++) {
                if (newClusterSize[i] > 1)
                    clusters[i][j] = newClusters[i][j] / newClusterSize[i];
                newClusters[i][j] = 0.0;
            }
            newClusterSize[i] = 0;
        }
            
        delta /= numObjs;
    } while (delta > threshold && loop++ < 500);

    if (_debug) {
        timing = omp_get_wtime() - timing;
        printf("nloops = %2d (T = %7.4f)\n", loop, timing);
    }
    
    free(newClusters[0]);
    free(newClusters);
    free(newClusterSize);

    return clusters;
}

