#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "kmeans.h"


__inline static
float euclid_dist_2(int numdims, float *coord1, float *coord2){
    float ans=0.0;

    for (int i=0; i<numdims; i++)
        ans += (coord1[i]-coord2[i]) * (coord1[i]-coord2[i]);

    return(ans);
}

/*----< find_nearest_cluster() >---------------------------------------------*/
__inline static
int find_nearest_cluster(int numClusters,int numCoords,float  *object, float **clusters){
    int index=0;
    float min_dist = euclid_dist_2(numCoords, object, clusters[0]); /* find the cluster id that has min distance to object */

    for (int i=1; i<numClusters; i++){
        float dist = euclid_dist_2(numCoords, object, clusters[i]);

        if (dist < min_dist) { /* find the min and its array index */
            min_dist = dist;
            index = i;
        }
    }
    return(index);
}

/*----< mpi_kmeans() >-------------------------------------------------------*/
int mpi_kmeans(float **objects, int numCoords, int numObjs, int numClusters, float threshold, int *membership, float **clusters, MPI_Comm  comm){
    int rank, index, loop=0, total_numObjs, *newClusterSize, *clusterSize;
    float delta, delta_tmp, **newClusters;
    extern int _debug;

    if (_debug)
        MPI_Comm_rank(comm, &rank);

    for (int i=0; i<numObjs; i++)
        membership[i] = -1;

    newClusterSize = (int*) calloc(numClusters, sizeof(int));
    assert(newClusterSize != NULL);
    clusterSize = (int*) calloc(numClusters, sizeof(int));
    assert(clusterSize != NULL);

    newClusters = (float**) malloc(numClusters * sizeof(float*));
    assert(newClusters != NULL);
    newClusters[0] = (float*) calloc(numClusters * numCoords, sizeof(float));
    assert(newClusters[0] != NULL);
    for (int i=1; i<numClusters; i++)
        newClusters[i] = newClusters[i-1] + numCoords;

    MPI_Allreduce(&numObjs, &total_numObjs, 1, MPI_INT, MPI_SUM, comm);
    if (_debug)
        printf("%2d: numObjs=%d total_numObjs=%d numClusters=%d numCoords=%d\n", rank, numObjs, total_numObjs, numClusters, numCoords);

    do{
        double curT = MPI_Wtime();
        delta = 0.0;
        for (int i=0; i<numObjs; i++){
            index = find_nearest_cluster(numClusters, numCoords, objects[i],clusters);

            if (membership[i] != index)
                delta += 1.0;

            membership[i] = index;

            newClusterSize[index]++;
            for (int j=0; j<numCoords; j++)
                newClusters[index][j] += objects[i][j];
        }

        MPI_Allreduce(newClusters[0], clusters[0], numClusters*numCoords,MPI_FLOAT, MPI_SUM, comm);
        MPI_Allreduce(newClusterSize, clusterSize, numClusters, MPI_INT,MPI_SUM, comm);

        for (int i=0; i<numClusters; i++){
            for (int j=0; j<numCoords; j++){
                if (clusterSize[i] > 1)
                    clusters[i][j] /= clusterSize[i];
                newClusters[i][j] = 0.0;
            }
            newClusterSize[i] = 0;
        }
            
        MPI_Allreduce(&delta, &delta_tmp, 1, MPI_FLOAT, MPI_SUM, comm);
        delta = delta_tmp / total_numObjs;

        if (_debug){
            double maxTime;
            curT = MPI_Wtime() - curT;
            MPI_Reduce(&curT, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, comm);
            if (rank == 0) printf("%2d: loop=%d time=%f sec\n",rank,loop,curT);
        }
    } while (delta > threshold && loop++ < 500);

    if (_debug && rank == 0)
        printf("%2d: delta=%f threshold=%f loop=%d\n",rank,delta,threshold,loop);

    free(newClusters[0]);
    free(newClusters);
    free(newClusterSize);
    free(clusterSize);

    return 1;
}

