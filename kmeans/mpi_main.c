#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <mpi.h>
int _debug;
#include "kmeans.h"

int mpi_kmeans(float**, int, int, int, float, int*, float**, MPI_Comm);
float** mpi_read(int, char*, int*, int*, MPI_Comm);
int mpi_write(int, char*, int, int, int, float**, int*, int, MPI_Comm);

/*---< usage() >------------------------------------------------------------*/
static void help(char *argv0, float threshold){
    char *help =
        "Usage: %s [switches] -i filename -n num_clusters\n"
        "       -i filename    : file containing data to be clustered\n"
        "       -b             : input file is in binary format (default no)\n"
        "       -r             : output file in binary format (default no)\n"
        "       -n num_clusters: number of clusters (K must > 1)\n"
        "       -t threshold   : threshold value (default %.4f)\n"
        "       -o             : output timing results (default no)\n"
        "       -d             : enable debug mode\n";
    fprintf(stderr, help, argv0, threshold);
}

/*---< main() >-------------------------------------------------------------*/
int main(int argc, char **argv){
    int opt, isInFileBinary, isOutFileBinary, isOutputTiming, isPrintUsage;
    extern char *optarg;
    extern int optind;
    int numClusters, numCoords, numObjs, totalNumObjs, rank, nproc, mpiNameLen;
    int *membership;
    char *filename, mpiName[MPI_MAX_PROCESSOR_NAME];
    float **objects, **clusters, threshold;
    double timing, ioTiming, clusteringTiming;
    MPI_Status status;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Get_processor_name(mpiName,&mpiNameLen);

    _debug = 0;
    threshold = 0.001;
    numClusters = 0;
    isInFileBinary = 0;
    isOutFileBinary = 0;
    isOutputTiming = 0;
    isPrintUsage = 0;
    filename = NULL;

    while ((opt=getopt(argc,argv,"p:i:n:t:abdorh"))!= EOF){
        switch (opt){
            case 'i':{
                filename=optarg;
                break;
            }
            case 'b':{
                isInFileBinary = 1;
                break;
            }
            case 'r':{
                isOutFileBinary = 1;
                break;
            }
            case 't':{
                threshold=atof(optarg);
                break;
            }
            case 'n':{
                numClusters = atoi(optarg);
                break;
            }
            case 'o':{
                isOutputTiming = 1;
                break;
            }
            case 'd':{
                _debug = 1;
                break;
            }
            case 'h':{
                isPrintUsage = 1;
                break;
            }
            default:{
                isPrintUsage = 1;
                break;
            }
        }
    }

    if (filename == 0 || numClusters <= 1 || isPrintUsage == 1){
        if (rank == 0) help(argv[0], threshold);
        MPI_Finalize();
        exit(1);
    }

    if (_debug)
        printf("Proc %d of %d running on %s\n", rank, nproc, mpiName);

    MPI_Barrier(MPI_COMM_WORLD);
    ioTiming = MPI_Wtime();

    objects = mpi_read(isInFileBinary, filename, &numObjs, &numCoords,
                       MPI_COMM_WORLD);

    if (_debug){
        int num = (numObjs < 4) ? numObjs : 4;
        for (int i=0; i<num; i++){
            char strline[1024], strfloat[16];
            sprintf(strline,"%d: objects[%d]= ",rank,i);
            for (int j=0; j<numCoords; j++) {
                sprintf(strfloat,"%10f",objects[i][j]);
                strcat(strline, strfloat);
            }
            strcat(strline, "\n");
            printf("%s",strline);
        }
    }

    timing = MPI_Wtime();
    ioTiming = timing - ioTiming;
    clusteringTiming = timing;

    clusters = (float**) malloc(numClusters * sizeof(float*));
    assert(clusters != NULL);
    clusters[0] = (float*) malloc(numClusters * numCoords * sizeof(float));
    assert(clusters[0] != NULL);
    for (int i=1; i<numClusters; i++)
        clusters[i] = clusters[i-1] + numCoords;

    MPI_Allreduce(&numObjs, &totalNumObjs, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    if (!rank){
        for (int i=0; i<numClusters; i++)
            for (int j=0; j<numCoords; j++)
                clusters[i][j] = objects[i][j];
    }
    MPI_Bcast(clusters[0], numClusters*numCoords, MPI_FLOAT, 0, MPI_COMM_WORLD);

    membership = (int*) malloc(numObjs * sizeof(int));
    assert(membership != NULL);

    mpi_kmeans(objects, numCoords, numObjs, numClusters, threshold, membership,
               clusters, MPI_COMM_WORLD);

    free(objects[0]);
    free(objects);

    timing = MPI_Wtime();
    clusteringTiming = timing - clusteringTiming;

    mpi_write(isOutFileBinary, filename, numClusters, numObjs, numCoords,
              clusters, membership, totalNumObjs, MPI_COMM_WORLD);

    free(membership);
    free(clusters[0]);
    free(clusters);

    if (isOutputTiming){
        double max_ioTiming, max_clusteringTiming;

        ioTiming += MPI_Wtime() - timing;

        MPI_Reduce(&ioTiming, &max_ioTiming, 1, MPI_DOUBLE,
                   MPI_MAX, 0, MPI_COMM_WORLD);
        MPI_Reduce(&clusteringTiming, &max_clusteringTiming, 1, MPI_DOUBLE,
                   MPI_MAX, 0, MPI_COMM_WORLD);

        if (!rank){
            printf("\nPerforming **** Simple Kmeans  (MPI) ****\n");
            printf("Num of processes = %d\n", nproc);
            printf("Input file:        %s\n", filename);
            printf("numObjs          = %d\n", totalNumObjs);
            printf("numCoords        = %d\n", numCoords);
            printf("numClusters      = %d\n", numClusters);
            printf("threshold        = %.4f\n", threshold);
            printf("I/O time           = %10.4f sec\n", max_ioTiming);
            printf("Computation timing = %10.4f sec\n", max_clusteringTiming);
        }
    }

    MPI_Finalize();
    return(0);
}

