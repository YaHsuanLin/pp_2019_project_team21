#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "kmeans.h"
#define DEBUG 0
int NUM_THREAD;

float** DELTA;
int* tmp_membership;
int     *newClusterSize; /* [numClusters]: no. objects assigned in each*/
float  **newClusters;    /* [numClusters][numCoords] */

pthread_mutex_t size_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t new_mutex = PTHREAD_MUTEX_INITIALIZER;

struct arg_struct
{
    int     numClusters; /* no. clusters */
    int     numCoords;   /* no. coordinates */
    int     numObjs;
    float **objects;
    float **clusters;    /* [numClusters][numCoords] */
    int id;
};

/*----< euclid_dist_2() >----------------------------------------------------*/
__inline static
float euclid_dist_2(int    numdims,  /* no. dimensions */
                    float *coord1,   /* [numdims] */
                    float *coord2)   /* [numdims] */
{
    int i;
    float ans=0.0;

    for (i=0; i<numdims; i++)
        ans += (coord1[i]-coord2[i]) * (coord1[i]-coord2[i]);

    return(ans);
}

/*----< find_nearest_cluster() >---------------------------------------------*/
__inline static
int find_nearest_cluster(int     numClusters, /* no. clusters */
                         int     numCoords,   /* no. coordinates */
                         float  *object,      /* [numCoords] */
                         float **clusters)    /* [numClusters][numCoords] */
{
    int   index, i;
    float dist, min_dist;

    /* find the cluster id that has min distance to object */
    index    = 0;
    min_dist = euclid_dist_2(numCoords, object, clusters[0]);

    for (i=1; i<numClusters; i++) {
        dist = euclid_dist_2(numCoords, object, clusters[i]);
        /* no need square root */
        if (dist < min_dist) { /* find the min and its array index */
            min_dist = dist;
            index    = i;
        }
    }
    return(index);
}

void* calc(void* arguments)
{
    int index, i, j;
    struct arg_struct *args = (struct arg_struct *)arguments;
    for (i=args->id; i<args->numObjs; i+=NUM_THREAD)
    {
        /* find the array index of nestest cluster center */
        index = find_nearest_cluster(args->numClusters, args->numCoords, args->objects[i], args->clusters);

        /* if membership changes, increase delta by 1 */
        if (tmp_membership[i] != index) DELTA[args->id][0] += 1.0;

        /* assign the membership to object i */
        tmp_membership[i] = index;

        /* update new cluster center : sum of objects located within */
        pthread_mutex_lock(&size_mutex);
        newClusterSize[index]++;
        pthread_mutex_unlock(&size_mutex);
        for (j=0; j<args->numCoords; j++)
        {
            pthread_mutex_lock(&new_mutex);
            newClusters[index][j] += args->objects[i][j];
            pthread_mutex_unlock(&new_mutex);
        }
    }
    return NULL;
}

/* return an array of cluster centers of size [numClusters][numCoords]       */
int pth_kmeans(int     nthreads,
               float **objects,      /* in: [numObjs][numCoords] */
               int     numCoords,    /* no. features */
               int     numObjs,      /* no. objects */
               int     numClusters,  /* no. clusters */
               float   threshold,    /* % objects change membership */
               int    *membership,   /* out: [numObjs] */
               float **clusters)     /* out: [numClusters][numCoords] */
{
    int      i, j, k, l, index, loop=0;
    float    delta;          /* % of objects change their clusters */
    NUM_THREAD = nthreads;

    /* initialize membership[] */
    tmp_membership = (int*) malloc(numObjs * sizeof(int));
    for (i=0; i<numObjs; i++) tmp_membership[i] = -1;

    /* need to initialize newClusterSize and newClusters[0] to all 0 */
    newClusterSize = (int*) calloc(numClusters, sizeof(int));
    assert(newClusterSize != NULL);

    newClusters    = (float**) malloc(numClusters *            sizeof(float*));
    assert(newClusters != NULL);
    newClusters[0] = (float*)  calloc(numClusters * numCoords, sizeof(float));
    assert(newClusters[0] != NULL);
    for (i=1; i<numClusters; i++)
        newClusters[i] = newClusters[i-1] + numCoords;

    DELTA = (float**) malloc(NUM_THREAD * sizeof(float*));
    for (i=0; i<NUM_THREAD; i++)
    {
        DELTA[i] = (float*) malloc(8 * sizeof(float));
        DELTA[i][0] = 0;
    }

    struct arg_struct args[NUM_THREAD];
    for (j=0; j<NUM_THREAD; j++)
    {
        args[j].numClusters = numClusters;
        args[j].numCoords = numCoords;
        args[j].numObjs = numObjs;
        args[j].objects = objects;
        args[j].id = j;
    }
    pthread_t threads[NUM_THREAD];
    do {
        delta = 0.0;
        for (j=0; j<NUM_THREAD; j++)
        {
            args[j].clusters = clusters;
            int t;
            t = pthread_create(&threads[j], NULL, calc, (void *)&args[j]);
            // if (DEBUG) printf("err[%d]: %d\n", t);
        }
        for (k=0; k<NUM_THREAD; k++)
        {
            pthread_join(threads[k], NULL);
            delta += DELTA[k][0];
        }

        /* average the sum and replace old cluster center with newClusters */
        for (i=0; i<numClusters; i++) {
            for (j=0; j<numCoords; j++) {
                if (newClusterSize[i] > 0)
                    clusters[i][j] = newClusters[i][j] / newClusterSize[i];
                newClusters[i][j] = 0.0;   /* set back to 0 */
            }
            newClusterSize[i] = 0;   /* set back to 0 */
        }
        for (i=0; i<NUM_THREAD; i++)
        {
            DELTA[i][0] = 0;
        }
        delta /= numObjs;
    } while (delta > threshold && loop++ < 500);

    for (i=0; i<numObjs; i++) membership[i] = tmp_membership[i];
    for (i=0; i<NUM_THREAD; i++)
    {
        free(DELTA[i]);
    }
    free(DELTA);
    free(tmp_membership);
    free(newClusters[0]);
    free(newClusters);
    free(newClusterSize);

    return 1;
}

